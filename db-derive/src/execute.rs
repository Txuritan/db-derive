use {
    crate::{sql::Sql, ConnTrans, Connection, Error, Transaction},
    std::convert::TryInto,
};

pub trait Execute<'__query>: Sql<'__query> {
    fn execute<C: TryInto<ConnTrans<'__query>>>(
        &'__query self,
        into_conn_trans: C,
    ) -> Result<u64, Error>
    where
        Error: From<C::Error>,
    {
        let conn_trans = into_conn_trans.try_into()?;

        match conn_trans {
            ConnTrans::Conn(conn) => match conn {
                #[cfg(feature = "postgresql")]
                Connection::PostgreSQL(mut conn) => {
                    let modified = conn.execute(
                        self.sql_postgres().as_ref(),
                        self.params_postgres().as_ref(),
                    )?;

                    Ok(modified)
                }
                #[cfg(feature = "sqlite")]
                Connection::SQLite(conn) => {
                    let modified =
                        conn.execute(self.sql_sqlite().as_ref(), self.params_sqlite().as_ref())?;

                    Ok(modified as u64)
                }
            },
            ConnTrans::Trans(trans) => match trans {
                #[cfg(feature = "postgresql")]
                Transaction::PostgreSQL(mut trans) => {
                    let modified = trans.execute(
                        self.sql_postgres().as_ref(),
                        self.params_postgres().as_ref(),
                    )?;

                    Ok(modified)
                }
                #[cfg(feature = "sqlite")]
                Transaction::SQLite(trans) => {
                    let modified =
                        trans.execute(self.sql_sqlite().as_ref(), self.params_sqlite().as_ref())?;

                    Ok(modified as u64)
                }
            },
        }
    }
}
