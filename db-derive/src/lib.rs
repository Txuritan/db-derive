pub use db_derive_impl::*;

#[cfg(feature = "postgresql")]
pub use postgres_types::to_sql_checked as postgres_to_sql_checked;

pub mod conn;
pub mod error;
pub mod execute;
pub mod kind;
pub mod pool;
pub mod query;
pub mod sql;
pub mod table;

pub use crate::{
    conn::{ConnTrans, Connection, Row, Transaction},
    error::{Error, OptionalExtension},
    pool::{Pool, PoolKind},
};

pub mod prelude {
    pub use crate::{
        execute::Execute,
        kind::Kind,
        query::Query,
        sql::Sql,
        table::{Schema, Table},
    };
}

pub mod internal {
    #[cfg(feature = "postgresql")]
    pub use {
        postgres::{
            types::{
                FromSql as PostgresFromSQL, IsNull as PostgresIsNull, Kind as PostgresKind,
                ToSql as PostgresToSQL, Type as PostgresType,
            },
            Row as PostgreRow,
        },
        postgres_types::private::BytesMut as PostgresBytesMut,
    };

    #[cfg(feature = "sqlite")]
    pub use rusqlite::{
        types::{
            FromSql as SQLiteFromSQL, FromSqlResult as SQLiteFromSqlResult, ToSql as SQLiteToSQL,
            ToSqlOutput as SQLiteToSqlOutput, ValueRef as SQLiteValueRef,
        },
        Result as SQLiteResult, Row as SQLiteRow,
    };
}
