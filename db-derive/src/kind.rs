#[cfg(feature = "postgresql")]
use crate::internal::{PostgresFromSQL, PostgresToSQL};
#[cfg(feature = "sqlite")]
use crate::internal::{SQLiteFromSQL, SQLiteToSQL};

#[cfg(not(feature = "sqlite"))]
pub trait Kind<'__from_sql>: PostgresFromSQL<'__from_sql> + PostgresToSQL {}

#[cfg(not(feature = "postgresql"))]
pub trait Kind<'__from_sql>: SQLiteFromSQL + SQLiteToSQL {}

#[cfg(all(feature = "postgresql", feature = "sqlite"))]
pub trait Kind<'__from_sql>:
    PostgresFromSQL<'__from_sql> + PostgresToSQL + SQLiteFromSQL + SQLiteToSQL
{
}
