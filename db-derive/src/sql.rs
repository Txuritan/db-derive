pub trait Sql<'__query> {
    type Sql: AsRef<str>;

    #[cfg(feature = "postgresql")]
    fn sql_postgres(&'__query self) -> Self::Sql;
    #[cfg(feature = "sqlite")]
    fn sql_sqlite(&'__query self) -> Self::Sql;

    #[cfg(feature = "postgresql")]
    type ParamsPostgres: AsRef<[&'__query (dyn postgres::types::ToSql + Sync)]>;
    #[cfg(feature = "sqlite")]
    type ParamsSQLite: AsRef<[&'__query dyn rusqlite::ToSql]>;

    #[cfg(feature = "postgresql")]
    fn params_postgres(&'__query self) -> Self::ParamsPostgres;
    #[cfg(feature = "sqlite")]
    fn params_sqlite(&'__query self) -> Self::ParamsSQLite;
}
