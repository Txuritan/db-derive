use crate::{
    internal::{PostgreRow, SQLiteRow},
    Error, Pool, PoolKind, Row,
};

pub trait Schema {
    fn schema(pool: &Pool) -> &'static str {
        match pool.as_kind() {
            #[cfg(feature = "postgresql")]
            PoolKind::PostgreSQL => Self::schema_postgres(),
            #[cfg(feature = "sqlite")]
            PoolKind::SQLite => Self::schema_sqlite(),
        }
    }

    /// The generated table schema for PostgreSQL.
    #[cfg(feature = "postgresql")]
    fn schema_postgres() -> &'static str;

    /// The generated table schema for SQLite.
    ///
    /// # Examples
    ///
    /// ```rust,no_run
    /// # use db_derive::{prelude::*, Connection};
    ///
    /// #[derive(db_derive::Table)]
    /// #[table(schema)]
    /// struct Tag {
    ///     #[table(rename = "Id")]
    ///     id: String,
    ///
    ///     #[table(rename = "Name")]
    ///     name: String,
    /// }
    ///
    /// assert_eq!("", Tag::schema_sqlite());
    /// ```
    #[cfg(feature = "sqlite")]
    fn schema_sqlite() -> &'static str;
}

/// The trait allows for the conversion of a resulting query's rows into the given implemented type.
///
/// # Examples
///
/// ```rust,no_run
/// # use db_derive::{prelude::*, Pool};
/// #[derive(db_derive::Table)]
/// struct Tag {
///     #[table(rename = "Id")]
///     id: String,
///
///     #[table(rename = "Name")]
///     name: String,
/// }
///
/// #[derive(db_derive::Query)]
/// #[query(sql = "SELECT Id, Name FROM Tag WHERE Id = $id;")]
/// struct GetTagById<'a> {
///     id: &'a str,
/// }
///
/// fn main() -> Result<(), db_derive::Error> {
///     let pool = Pool::sqlite("example.db")?;
///
///     let get = GetTagById { id: "example" };
///
///     let tag = get.query_row::<_, Tag>(&pool)?;
///
///     println!("Tag Name: {}", tag.name);
///
///     Ok(())
/// }
/// ```
pub trait Table {
    fn columns() -> usize;

    /// Internally used to transform the resulting query's rows into their given table type.
    /// This is just a mapper to the row transform implementations for each database.
    ///
    /// # Error
    ///
    /// This will return with an [`Error`] if a row column is either the wrong type
    /// or is unable to parse a column.
    ///
    /// [`Error`]: db_derive::Error
    fn from_row(row: Row<'_>) -> Result<Self, Error>
    where
        Self: Sized,
    {
        match row {
            #[cfg(feature = "postgresql")]
            Row::PostgreSQL(row) => Self::from_row_postgres(row),
            #[cfg(feature = "sqlite")]
            Row::SQLite(row) => Self::from_row_sqlite(row),
        }
    }

    /// The PostgreSQL row transform implementation.
    ///
    /// # Error
    ///
    /// This will return with an [`Error`] if a row column is either the wrong type
    /// or is unable to parse a column.
    ///
    /// [`Error`]: db_derive::Error
    #[cfg(feature = "postgresql")]
    fn from_row_postgres(row: PostgreRow) -> Result<Self, Error>
    where
        Self: Sized;

    /// The SQLite row transform implementation.
    ///
    /// # Error
    ///
    /// This will return with an [`Error`] if a row column is either the wrong type
    /// or is unable to parse a column.
    ///
    /// [`Error`]: db_derive::Error
    #[cfg(feature = "sqlite")]
    fn from_row_sqlite<'__table_row>(
        row: &'__table_row SQLiteRow<'__table_row>,
    ) -> Result<Self, Error>
    where
        Self: Sized;
}
