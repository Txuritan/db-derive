use db_derive::{prelude::*, Pool};

#[derive(Debug, db_derive::Kind)]
pub enum TagType {
    #[kind(rename = "warning")]
    Warning,

    #[kind(rename = "pairing")]
    Pairing,

    #[kind(rename = "character")]
    Character,

    #[kind(rename = "general")]
    General,
}

#[derive(db_derive::Table)]
#[table(exists, schema)]
pub struct Tag {
    #[table(rename = "Id", primary, length = 10)]
    pub id: String,

    #[table(rename = "Name", unique, length = 256)]
    pub name: Option<String>,
}

#[derive(db_derive::Execute)]
#[execute(sql = "UPDATE Tag SET Name = {name} WHERE Id = {id};")]
pub struct UpdateTagName<'a> {
    id: &'a str,
    name: &'a str,
}

impl<'a> UpdateTagName<'a> {
    pub fn build(id: &'a str, name: &'a str) -> UpdateTagName<'a> {
        UpdateTagName { id, name }
    }
}

#[derive(db_derive::Query)]
#[query(sql = "SELECT Id, Name FROM Tag WHERE Id = {id};")]
pub struct GetTagById<'a> {
    id: &'a str,
}

pub fn get_tag_name() {
    let pool = Pool::sqlite("test.db").unwrap();

    let get = GetTagById { id: "example" };

    let tag = get.query_row::<_, Tag>(&pool).unwrap();

    assert_eq!("test", tag.name.unwrap().as_str());
}

#[derive(db_derive::Query)]
#[query(sql = "UPDATE Tag SET Name = {name} WHERE Id = {id};")]
pub struct SetTagNameById<'a> {
    id: &'a str,
    name: &'a str,
}

pub fn set_tag_name() {
    let pool = Pool::sqlite("test.db").unwrap();

    pool.transaction(|mut trans| {
        trans.schema::<Tag>()?;

        Ok(())
    })
    .unwrap();

    pool.transaction(|trans| {
        let set = SetTagNameById {
            id: "example",
            name: "new test",
        };

        let _ = set.query_row::<_, Tag>(trans)?;

        Ok(())
    })
    .unwrap();

    let get = GetTagById { id: "example" };

    let tag = get.query_row::<_, Tag>(&pool).unwrap();

    assert_eq!("new test", tag.name.unwrap().as_str());
}
