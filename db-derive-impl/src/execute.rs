use {
    crate::sql,
    darling::{ast::Data, util::Ignored, FromDeriveInput},
    proc_macro2::TokenStream,
    syn::{DeriveInput, Generics, Ident},
};

#[derive(Debug, darling::FromDeriveInput)]
#[darling(attributes(execute), supports(struct_named))]
pub struct Execute {
    #[darling(default)]
    sql: Option<String>,

    #[darling(default)]
    postgres: Option<String>,

    #[darling(default)]
    sqlite: Option<String>,

    data: Data<Ignored, ExecuteField>,
    generics: Generics,
}

#[derive(Debug, darling::FromField)]
pub struct ExecuteField {
    ident: Option<Ident>,
}

impl Execute {
    // pub fn function(_input: DeriveInput) -> TokenStream {
    //     quote::quote! {}
    // }

    pub fn derive(input: DeriveInput) -> TokenStream {
        let query = Execute::from_derive_input(&input).unwrap();

        let mut new_generics = query.generics.clone();

        new_generics.params.push(syn::parse_quote!('__query));

        let (_, type_generics, _) = query.generics.split_for_impl();
        let (impl_generics, _, where_clause) = new_generics.split_for_impl();

        let ident = &input.ident;

        let sql = sql::new_impl(
            ident,
            query.generics.clone(),
            query.sql,
            query.postgres,
            query.sqlite,
        );

        quote::quote! {
            #sql

            impl #impl_generics ::db_derive::prelude::Execute<'__query> for #ident #type_generics #where_clause {}
        }
    }
}
