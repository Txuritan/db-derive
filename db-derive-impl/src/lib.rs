extern crate proc_macro;

mod execute;
mod kind;
mod parser;
mod query;
mod sql;
mod table;

use {
    crate::{execute::Execute, kind::Kind, query::Query, table::Table},
    proc_macro::TokenStream,
};

// #[proc_macro]
// pub fn execute(input: TokenStream) -> TokenStream {
//     let input = syn::parse(input).unwrap();
//
//     Execute::function(input).into()
// }

#[proc_macro_derive(Execute, attributes(execute))]
pub fn derive_execute(input: TokenStream) -> TokenStream {
    let input = syn::parse(input).unwrap();

    Execute::derive(input).into()
}

#[proc_macro_derive(Kind, attributes(kind))]
pub fn derive_kind(input: TokenStream) -> TokenStream {
    let input = syn::parse(input).unwrap();

    Kind::derive(input).into()
}

// #[proc_macro]
// pub fn query(input: TokenStream) -> TokenStream {
//     let input = syn::parse(input).unwrap();
//
//     Query::function(input).into()
// }

#[proc_macro_derive(Query, attributes(query))]
pub fn derive_query(input: TokenStream) -> TokenStream {
    let input = syn::parse(input).unwrap();

    Query::derive(input).into()
}

#[proc_macro_derive(Table, attributes(table))]
pub fn derive_table(input: TokenStream) -> TokenStream {
    let input = syn::parse(input).unwrap();

    Table::derive(input).into()
}
